//
//  MMCloud.h
//  MMMemegen
//
//  Created by Josh Berlin on 12/10/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

// The interface to the Cloud storage system for all of the uploaded memes.
@interface MMCloud : NSObject

// Forms a Cloud in the digital sky to begin collecting meme images.
+ (void)beginCollectingCondensationWithUsername:(NSString *)username
                                       andEmail:(NSString *)email;

// Uploads a meme to the Cloud.
+ (void)uploadImage:(UIImage *)image onComplete:(void (^)())onComplete;
  
@end
