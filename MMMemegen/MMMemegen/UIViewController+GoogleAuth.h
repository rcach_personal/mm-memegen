//
//  MMAppDelegate+GoogleAuth.h
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/13/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "MMAppDelegate.h"

@interface UIViewController (GoogleAuth)

- (BOOL)isLoggedIn;
- (void)presentLoginViewController;
- (NSString *)loggedInUserEmail;
- (void)logout;

@end
