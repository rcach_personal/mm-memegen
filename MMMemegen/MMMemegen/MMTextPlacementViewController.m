//
//  MMTextPlacementViewController.m
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/8/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "MMTextPlacementViewController.h"

#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>

#import "MMCloud.h"
#import "MMTextPlacementViewControllerConstraintManager.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTMOAuth2Authentication.h"
#import "UIImageView+MMContentScale.h"
#import "UIView+Additions.h"

@interface MMTextPlacementViewController ()
@property (weak, nonatomic) IBOutlet UIView *memeView;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *memeTextPanGestureRecognizer;
@property (nonatomic, assign) CGPoint memeLabelTouchPointOffset;
@property (weak, nonatomic) IBOutlet UIImageView *memeImageView;
@property (weak, nonatomic) IBOutlet UIView *savingView;
@property (nonatomic, strong) MMTextPlacementViewControllerConstraintManager *constraintManager;
@end

@implementation MMTextPlacementViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    self.hidesBottomBarWhenPushed = YES;
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  self.memeImageView.image = self.memeImage;
  self.memeLabel.text = self.memeText;
  self.savingView.layer.cornerRadius = 10.0f;
  
  self.constraintManager = [[MMTextPlacementViewControllerConstraintManager alloc] initWithTextPlacementViewController:self];
}

- (void)viewWillAppear:(BOOL)animated {
  [self.constraintManager setUpMemeLabelConstraints];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  [self.constraintManager setUpMemeImageViewConstraints];
}

- (IBAction)handleMemeTextPan:(UIPanGestureRecognizer *)panGestureRecognizer {
  CGPoint touchPoint = [panGestureRecognizer locationInView:self.memeView];
  switch (panGestureRecognizer.state) {
    case UIGestureRecognizerStateBegan:
      self.memeLabelTouchPointOffset = CGPointMake(self.memeLabel.center.x - touchPoint.x ,
                                                   self.memeLabel.center.y - touchPoint.y);
      break;

    case UIGestureRecognizerStateChanged:
      self.memeLabel.center = CGPointMake(self.memeLabelTouchPointOffset.x + touchPoint.x ,
                                          self.memeLabelTouchPointOffset.y + touchPoint.y);
      break;

    case UIGestureRecognizerStateEnded:
      break;

    default:
      break;
  }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
  CGPoint touchPoint = [gestureRecognizer locationInView:self.memeView];
  return CGRectContainsPoint(self.memeLabel.frame, touchPoint);
}

- (IBAction)share:(id)sender {
  UIImage *image = [self.memeView takeScreenshot];

  self.savingView.hidden = NO;

  [MMCloud uploadImage:image onComplete:^{
      [self.navigationController popToRootViewControllerAnimated:YES];
  }];
}

@end
