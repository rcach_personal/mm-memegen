//
//  MMTextPlacementViewControllerConstraintManager.m
//  MMMemegen
//
//  Created by Josh Berlin on 1/4/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "MMTextPlacementViewControllerConstraintManager.h"

#import "MMTextPlacementViewController.h"

@interface MMTextPlacementViewControllerConstraintManager ()
@property(nonatomic) CGRect bounds;
@property(nonatomic, strong) UIImage *memeImage;
@property(nonatomic, strong) UILabel *memeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelWidthConstraint;
@end

@implementation MMTextPlacementViewControllerConstraintManager

- (id)initWithTextPlacementViewController:(MMTextPlacementViewController *)textPlacementViewController {
  self = [super init];
  if (self) {
    self.bounds = textPlacementViewController.view.bounds;
    self.memeImage = textPlacementViewController.memeImage;
    self.memeLabel = textPlacementViewController.memeLabel;
    self.imageViewHeightConstraint = textPlacementViewController.imageViewHeightConstraint;
    self.imageViewWidthConstraint = textPlacementViewController.imageViewWidthConstraint;
    self.labelHeightConstraint = textPlacementViewController.memeLabelHeightConstraint;
  }
  return self;
}

- (void)setUpMemeImageViewConstraints {
  if (self.bounds.size.height > self.bounds.size.width) {
    self.imageViewWidthConstraint.constant = self.bounds.size.width;
    CGFloat aspectRatio = self.memeImage.size.height / self.memeImage.size.width;
    CGFloat height = self.bounds.size.width * aspectRatio;
    self.imageViewHeightConstraint.constant = height;
  } else {
    self.imageViewHeightConstraint.constant = self.bounds.size.height;
    CGFloat aspectRatio = self.memeImage.size.width / self.memeImage.size.height;
    CGFloat width = self.bounds.size.height * aspectRatio;
    self.imageViewWidthConstraint.constant = width;
  }
}

- (void)setUpMemeLabelConstraints {
  if (self.bounds.size.height > self.bounds.size.width) {
    self.labelWidthConstraint.constant = self.bounds.size.width - 20.0f;
    CGSize aSize = [self.memeLabel sizeThatFits:CGSizeMake((self.bounds.size.width - 20.0f), self.bounds.size.height)];
    self.labelHeightConstraint.constant = aSize.height;
  } else {
    CGFloat aspectRatio = self.memeImage.size.width / self.memeImage.size.height;
    CGFloat width =  self.bounds.size.height * aspectRatio;
    self.labelWidthConstraint.constant = width - 20.0f;
    CGSize aSize = [self.memeLabel sizeThatFits:CGSizeMake((width - 20.0f), self.bounds.size.height)];
    self.labelHeightConstraint.constant = aSize.height;
  }
}

@end
