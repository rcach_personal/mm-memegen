//
//  MMAppDelegate+GoogleAuth.m
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/13/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "UIViewController+GoogleAuth.h"

#import "GTMOAuth2ViewControllerTouch.h"
#import "GTMOAuth2Authentication.h"


static NSString * const kCLIENT_ID = @"581129179651.apps.googleusercontent.com";
static NSString * const kCLIENT_SECRET = @"edT1snhlorOlE6r8xsTHIt8_";
static NSString * const kKEYCHAIN_CREDENTIAL_KEY = @"com.mutualmobile.MMMemegen";

@implementation UIViewController (GoogleAuth)

- (GTMOAuth2Authentication *)authObject {
  return
    [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKEYCHAIN_CREDENTIAL_KEY
                                                          clientID:kCLIENT_ID
                                                      clientSecret:kCLIENT_SECRET];
}

- (BOOL)isLoggedIn {
    if ([self authObject].canAuthorize) {
    return YES;
  } else {
    return NO;
  }
}

- (void)logout {
  if ([[self authObject].serviceProvider isEqual:kGTMOAuth2ServiceProviderGoogle]) {
    // remove the token from Google's servers
    [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:[self authObject]];
  }

  // remove the stored Google authentication from the keychain, if any
  [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKEYCHAIN_CREDENTIAL_KEY];
}

- (void)presentLoginViewController {
  GTMOAuth2ViewControllerTouch *viewController =
    [GTMOAuth2ViewControllerTouch controllerWithScope:@""
                                             clientID:kCLIENT_ID
                                         clientSecret:kCLIENT_SECRET
                                     keychainItemName:kKEYCHAIN_CREDENTIAL_KEY
                                    completionHandler:^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error){
                                        NSLog(@"Auth: %@", viewController.authentication.userEmail);
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                    }];
    
    [self presentViewController:viewController animated:YES completion:nil];
}

- (NSString *)loggedInUserEmail {
  return [self authObject].userEmail;
}


@end
