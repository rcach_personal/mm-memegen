//
//  MMTextPlacementViewController.h
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/8/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMTextPlacementViewController : UIViewController

@property(nonatomic, strong) UIImage *memeImage;
@property(nonatomic, strong) NSString *memeText;
@property (weak, nonatomic) IBOutlet UILabel *memeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memeLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;

@end
