//
//  MMAppDelegate.h
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/8/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
