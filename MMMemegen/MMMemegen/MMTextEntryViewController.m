//
//  MMTextEntryViewController.m
//  MMMemegen
//
//  Created by Josh Berlin on 11/12/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import "MMTextEntryViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "MMTextPlacementViewController.h"

@interface MMTextEntryViewController ()<UITextViewDelegate>

@property(nonatomic, weak) IBOutlet UIImageView *memeImageView;
@property(nonatomic, weak) IBOutlet UITextView *memeTextView;

@end

@implementation MMTextEntryViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.memeImageView.image = self.memeImage;
  self.memeTextView.layer.cornerRadius = 4.0f;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [self.memeTextView becomeFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"textPlacementSegue"]) {
    MMTextPlacementViewController *textPlacementViewController =
        (MMTextPlacementViewController *)segue.destinationViewController;
    textPlacementViewController.memeImage = self.memeImage;
    textPlacementViewController.memeText = self.memeTextView.text;
  }
}

@end
