//
//  MMTextEntryViewController.h
//  MMMemegen
//
//  Created by Josh Berlin on 11/12/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMTextEntryViewController : UIViewController

@property(nonatomic, strong) UIImage *memeImage;

@end
