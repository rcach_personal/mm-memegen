//
//  main.m
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/8/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([MMAppDelegate class]));
  }
}
