#import "MMMemeImages.h"

static NSArray *memeImages;

@implementation MMMemeImages

+ (void)initialize {
  if (self == [MMMemeImages class]) {
    memeImages = @[[UIImage imageNamed:@"doublefacepalm.jpg"],
                   [UIImage imageNamed:@"victorybaby.jpg"],
                   [UIImage imageNamed:@"xx.jpg"],
                   [UIImage imageNamed:@"groundhog_day.jpg"]];
  }
}

+ (NSUInteger)numberOfImages {
  return [memeImages count];
}

+ (UIImage*)imageAtIndexPath:(NSIndexPath*)indexPath { 
  return memeImages[indexPath.row];
}

+ (CGSize)smallSizeForImageAtIndexPath:(NSIndexPath*)indexPath {
  UIImage *memeImage = [MMMemeImages imageAtIndexPath:indexPath];
  CGFloat aspectRatio = memeImage.size.width / memeImage.size.height;
  CGFloat height = 100.0f;
  CGFloat width =  height * aspectRatio;
  return CGSizeMake(width, height);
}


@end
