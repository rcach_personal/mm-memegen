//
//  UIView+Additions.m
//  MMMemegen
//
//  Created by Josh Berlin on 1/4/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import "UIView+Additions.h"

#import <QuartzCore/QuartzCore.h>

@implementation UIView (MMAdditions)

- (UIImage *)takeScreenshot {
  UIGraphicsBeginImageContext(self.bounds.size);
  [self.layer renderInContext:UIGraphicsGetCurrentContext()];
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

@end
