@interface MMMemeImages : NSObject
+ (NSUInteger)numberOfImages;
+ (UIImage*)imageAtIndexPath:(NSIndexPath*)indexPath;
+ (CGSize)smallSizeForImageAtIndexPath:(NSIndexPath*)indexPath;
@end
