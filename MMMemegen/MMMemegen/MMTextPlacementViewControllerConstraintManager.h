//
//  MMTextPlacementViewControllerConstraintManager.h
//  MMMemegen
//
//  Created by Josh Berlin on 1/4/13.
//  Copyright (c) 2013 Mutual Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMTextPlacementViewController;

@interface MMTextPlacementViewControllerConstraintManager : NSObject

- (id)initWithTextPlacementViewController:(MMTextPlacementViewController *)textPlacementViewController;

- (void)setUpMemeImageViewConstraints;
- (void)setUpMemeLabelConstraints;

@end
