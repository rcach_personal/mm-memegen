#import "MMImagePickerViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "MMTextEntryViewController.h"
#import "UIViewController+GoogleAuth.h"
#import "MMImageCell.h"
#import "MMMemeImages.h"

static NSString * textEntrySegueIdentifier = @"textEntrySegue";

@interface MMImagePickerViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *userEmailLabel;

@end

@implementation MMImagePickerViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  if (![self isLoggedIn]) {
    [self presentLoginViewController];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  self.userEmailLabel.text = [self loggedInUserEmail];
}

- (IBAction)signOut:(id)sender {
  [self logout];
  [self presentLoginViewController];
}

#pragma mark UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [MMMemeImages numberOfImages];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
  imageCell.layer.cornerRadius = 4.0f;
  UIImage *memeImage = [MMMemeImages imageAtIndexPath:indexPath];
  imageCell.imageView.image = memeImage;
  return imageCell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = (MMImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
  imageCell.imageView.alpha = 0.5f;
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = (MMImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
  imageCell.imageView.alpha = 1.0f;
}

#pragma mark UICollectionViewDataSource

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = (MMImageCell*)[collectionView cellForItemAtIndexPath:indexPath];
  imageCell.imageView.alpha = 0.5f;
  UIImage *memeImage = imageCell.imageView.image;
  [self performSegueWithIdentifier:textEntrySegueIdentifier sender:memeImage];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = (MMImageCell *)[collectionView cellForItemAtIndexPath:indexPath];
  imageCell.imageView.alpha = 1.0f;
}

#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return [MMMemeImages smallSizeForImageAtIndexPath:indexPath];
}

#pragma mark Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:textEntrySegueIdentifier]) {
    MMTextEntryViewController *textEntryViewController = (MMTextEntryViewController *)segue.destinationViewController;
    UIImage *memeImage = (UIImage *)sender;
    textEntryViewController.memeImage = memeImage;
  }
}

@end
